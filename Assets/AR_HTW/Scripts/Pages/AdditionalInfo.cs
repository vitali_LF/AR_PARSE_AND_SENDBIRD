﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Parse;

public class AdditionalInfo : MonoBehaviour {
	public InputField devName;
    public Dropdown devType; 
	public Dropdown clientOrServer; 
    public ParseUser user; 
    public GuiManager guiManager;

	public void Done()
    {
        user = ParseUser.CurrentUser;
        if(user==null){
            Debug.LogError("USER NOT FOUND !!!!!");
            //show log in page
        }

        user["devName"] = devName.text;
        user["devType"] = devType.captionText.text;
        user["Battery"] = "100%";
        user["Model"] = "IPhone 6"; 
        user["clientOrServer"] = clientOrServer.captionText.text;

        user.SaveAsync().ContinueWith(task => {
            if(task.IsCompleted && (task.IsFaulted || task.IsCanceled)){
                Debug.LogError("Cant save additional user data");
            }
            else{
                Debug.Log("Additional user data saved"); 
                guiManager.ShowUserHomePage();
            }
        });
	}
}
