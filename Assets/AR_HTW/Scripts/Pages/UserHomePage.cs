﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Parse;
using SendBird; 

public class UserHomePage : MonoBehaviour {


    private ParseUser user = null;  

    void OnEnable(){
        user = ParseUser.CurrentUser;
        if (user == null)
        {
            GuiManager.Instance.ShowLoginPage();
            return;
        }  
        string username = user.Username;
        SendBirdClient.Connect(username, (User user, SendBirdException e) =>
        {
            if(e != null)
            { 
                Debug.LogError("Can't coonect to SendBird"+e.Message);
                return;
            }
        });

    }

}
