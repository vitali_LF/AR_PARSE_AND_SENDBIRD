﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Parse; 

public class UserStatsPage : MonoBehaviour {


    private ParseUser user = null; 
    public Text userName;
    public Text devName;
    public Text devType; 
    public Text battery; 

    void OnEnable(){
        user = ParseUser.CurrentUser;
        if (user == null)
        {
            GuiManager.Instance.ShowLoginPage();
            return;
        } 
        if (userName == null || devName == null || devType == null)
            return; 
        userName.text = user.Username;
        devName.text = user.Get<string>("devName");
        devType.text = user.Get<string>("devType");
        battery.text = user.Get<string>("Battery");


    }

}
