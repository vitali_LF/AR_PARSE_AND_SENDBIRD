﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SendBird;

public class SendBirdInitializer : MonoBehaviour {

    public string appID;
    void Awake() {
        SendBirdClient.SetupUnityDispatcher (gameObject); // Set SendBird gameobject to DontDestroyOnLoad.
        StartCoroutine (SendBirdClient.StartUnityDispatcher); // Start a Unity dispatcher.
        SendBirdClient.Init (appID); // SendBird Sample Application ID

    }
    void OnDestroy() {
        Debug.LogWarning("OnDestroy SendBirdInitializer");
        SendBirdClient.Disconnect(() => {
            Debug.LogWarning("SendBird disconected");
        });
    }
}
