﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using SendBird;
using Parse;

public class SB_SendMessage : MonoBehaviour {
 
    public InputField userID;
    public InputField message;
    private GroupChannel groupChannel;

	public void Invite () {
        List<string> ids = new List<string>();
        ids.Add(userID.text); 
        GroupChannel.CreateChannelWithUserIds(ids, true, ParseUser.CurrentUser.Username, null, null, (GroupChannel gChannel, SendBirdException e) => {
            if (e != null) {
                Debug.LogError("Can't coonect to SendBird"+e.Message);
                return; 
            }
            groupChannel = gChannel;
            //groupChannel. = ;
            Debug.Log("Chat created: "+groupChannel.CreatedAt + " " + groupChannel.Name);
        }); 
	} 

    public void SendMsg(){
        if(groupChannel==null){ 
            Debug.LogError(" CREATE A CHAT FIRST, TO BE ABLE TO SEND MESSAGES");
        }
        groupChannel.SendUserMessage(message.text, null, (UserMessage userMessage, SendBirdException e) => {
            if (e != null) {
                Debug.LogError("Can't send message over SendBird" +e.Message);
                return; 
            }
        });
    }


}
