﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SendBird;
using Parse;


public class SB_ReceiveMessages : MonoBehaviour {

    public Text channelList; 
    public Text messageBoard; 
    public Text myName;

    void Start(){

        var user = ParseUser.CurrentUser;
        myName.text = user.Username;
    }

    public void GetMyChannelsList(){
        GroupChannelListQuery mQuery = GroupChannel.CreateMyGroupChannelListQuery();
        mQuery.IncludeEmpty = true;
        mQuery.Next((List<GroupChannel> list, SendBirdException e) => {
            if (e != null) {
                Debug.LogError("Can't get list of channels: "+e.Message);
                return; 
            }
            channelList.text = "";  
            messageBoard.text = "";  
            foreach (var channel in list) { 
                AttachInfoAboutChannel(channel);

            }
            SendBirdClient.ChannelHandler ch = new SendBirdClient.ChannelHandler();
            ch.OnMessageReceived = (BaseChannel baseChannel, BaseMessage baseMessage) => {
                var userMessage = (UserMessage) baseMessage;
                messageBoard.text += "[" + baseMessage.CreatedAt+"]" + "[" + userMessage.Sender+"]" +  " " + userMessage.Message + "\n";
            };
            SendBirdClient.AddChannelHandler("handler_lol", ch);

        });
    }

    void AttachInfoAboutChannel(GroupChannel channel)
    {
        channelList.text += channel.Name;
        channelList.text += "(";
        List<User> members = channel.Members;
        foreach (var member in members)
        {
            channelList.text += member.UserId + ",";
        }
        channelList.text += ")\n";
    }
}
